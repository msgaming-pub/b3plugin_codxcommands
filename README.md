# README #

### What is this repository for? ###

* A basic wrapper for codx commands translated to b3 commands.
* Version 1.0

### How do I get set up? ###

* Add `codxcommands.py` to `extplugins/`
* Add `plugin_codxcommands.xml` to `extplugins/conf/`

* Add 	`<plugin name="codxcommands" config="@b3/extplugins/conf/plugin_codxcommands.xml" />` to your b3 config file.

### Contribution guidelines ###

* Feel free to add more of the codx commands.

### Who do I talk to? ###

* For more information please contact admin@ms-gaming.com

or visit us at http://www.ms-gaming.com